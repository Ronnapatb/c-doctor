var app = require('express')(); 
var port = process.env.PORT || 7777;
var mongojs = require('./db');
var db = mongojs.connect;
var bodyParser = require('body-parser')
var morgan = require('morgan')
var cors = require('cors');   
app.use(cors());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(morgan('combined'))
app.get('/', function (req, res) {
    var intent = {
            name:"welcome",
            followUpIntent: null,
            type: "welcome",
            diseaseResult: null,
            root:null,
            contextInput:"",
            contextOutput:"",
            trainingData: [],
            parameter:[],
            responseMessage:[],
            endConver: false,
            sendApi:false
    };

    db.intent.insert(intent, function(err, docs) {
        console.log("complete")
        res.send('Add new ' + docs.class + ' Completed!');
    });
    
});
app.get('/getAllIntent/:type',function (req,res){
    var type = req.params.type;
    if(type == {}){
        db.intent.find({ type: { $nin: ["fallback","welcome"] } }).toArray(function(err, docs) {
            console.log(docs);
            res.send(docs);
        });
    }
    else{
        db.intent.find({ type: type }).toArray(function(err, docs) {
            console.log(docs);
            res.send(docs);
        });
    }

})
app.post('/addNewIntent',function(req,res){
    var intentReq = req.body;
    db.intent.insert(intentReq, function(err,data){
        console.log(data);
        res.send(data);
        
    })
});
app.post('/addNewFollowupIntent',function(req,res){
    var intentReq = req.body;
    var newfollowUpIntent = {
      name:"newIntent",
      followUpIntent:[],
      type: "custom",
      diseaseResult: null,
      root:false,
      contextInput:"",
      contextOutput:"",
      trainingData:[],
      parameter:[
      {
        variable:"",
        value: "",
      }],
      responseMessage:[],
      endConver: false,
      sendApi:false
    }
    
    if(intentReq.contextOutput == ""){
        intentReq.contextOutput = intentReq.name.replace(/\s/g,'') + "-followup"
        newfollowUpIntent.contextInput = intentReq.contextOutput
      }
    else{
        newfollowUpIntent.contextInput = intentReq.contextOutput
    }
    newfollowUpIntent.name = intentReq.name + "-sub"
    db.intent.insert(newfollowUpIntent, function(err,data){
        res.send(data)
    })
 
});
app.post('/updateRootFollowupIntent',function (req,res){
    var intentReq = req.body;
    console.log(intentReq)
    console.log(intentReq.followUpIntent)
    var id = mongojs.ObjectId(intentReq._id)
    var updateVar= {
        "followUpIntent": intentReq.followUpIntent
    }
    intentReq._id = mongojs.ObjectId(intentReq._id)
    console.log(updateVar)
    console.log(id)
    db.intent.update(
        { "_id" : id },
        { $set: intentReq},
        () => {
        }
     );
    
});
app.post('/deleteIntent',function (req,res){
    var data = req.body
    console.log("delete")
    console.log(data)
    var dataObject = []
    data.forEach((element) => {
        dataObject.push(mongojs.ObjectId(element))
    });
    console.log(dataObject)
    db.intent.remove({'_id':{'$in':dataObject}},()=>{
        res.sendStatus(200)
    })
      
})
app.post('/UpdateIntent',function (req,res){
    var data = req.body
    console.log(data)
    var id = mongojs.ObjectId(data._id)
    delete data._id;
    db.intent.update(
        { "_id" : id },
        { $set: data},
        (data) => {
            console.log(data)
        }
     );

})
app.post('/getIntent', function (req, res) {
    var context = req.body;
    context.type = { $nin: ["fallback","welcome","result"] }
    console.log(context)
    db.intent.find(context).toArray(function(err, docs) {
        console.log(docs);
        res.send(docs);
    });

});
app.get('/getResult/:diseaseResult', function (req, res) {
    var diseaseResult = req.params.diseaseResult;
    db.intent.findOne({ "diseaseResult" : diseaseResult},function (err, response) {
        console.log(response);
        res.send(response)
      });

});
app.get('/getFallbackIntent', function (req, res) {
    db.intent.findOne({ "type" : "fallback"},function (err, response) {
        console.log(response);
        res.send(response)
      });

});

app.get('/index', function (req, res) {
    res.send('<h1>This is index page</h1>');
});

app.listen(port, function() {
    console.log('Starting node.js on port ' + port);
});