var mongojs = require('mongojs');

var databaseUrl = 'intent';
var collections = ['intent'];

var connect = mongojs(databaseUrl, collections);
var ObjectId = mongojs.ObjectID
module.exports = {
    connect: connect,
    ObjectId: ObjectId
};