#!/usr/bin/env python
# coding: utf-8

# In[109]:


from sklearn.externals import joblib 
import scipy.sparse as sp
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.decomposition import TruncatedSVD
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.naive_bayes import GaussianNB, BernoulliNB, MultinomialNB
from itertools import chain
from pythainlp import word_tokenize
import numpy as np
import pandas as pd
import sys
import json

mnb = joblib.load('test.pkl')
vocabulary_ = joblib.load('vocab.pkl')
labels = joblib.load('labels.pkl')


# In[100]:


def tokenize_text_newmm(l):
    """Tokenize list of text"""
    return list(chain.from_iterable([word_tokenize(l, engine='newmm') ]))
from pythainlp.corpus import stopwords

stop_word = stopwords.words('thai')
other = ['\r',' ',"ครับ","ค่ะ","คะ","ค่า","คับ","ครัช","นะ"]
def clean_list(tokenized_texts):

    i = 0

    for i in range(len( tokenized_texts)):
        if (i % 10 == 0):
            print("Cleaned "+ str(i) +" lines ")
        # tokenized_texts[i] = [word for word in tokenized_texts[i] if word not in stop_word]
        tokenized_texts[i] = [word for word in tokenized_texts[i] if word not in other]

# In[101]:


def text_to_bow(tokenized_text, vocabulary_):
  
    n_doc = len(tokenized_text)
    values, row_indices, col_indices = [], [], []
    for r, tokens in enumerate(tokenized_text):
        feature = {}
        for token in tokens:
            word_index = vocabulary_.get(token)
            if word_index is not None:
                if word_index not in feature.keys():
                    feature[word_index] = 1
                else:
                    feature[word_index] += 1
        for c, v in feature.items():
            values.append(v)
            row_indices.append(r)
            col_indices.append(c)

    # document-term matrix in sparse CSR format
    X = sp.csr_matrix((values, (row_indices, col_indices)),
                      shape=(n_doc, len(vocabulary_)))
    return X


# In[102]:


def predictTest(Text):
    test_Sentence_token = word_tokenize(Text, engine='newmm') 
    test_Sentence_token_clean = clean_list(test_Sentence_token)
    text_test = text_to_bow([test_Sentence_token_clean], vocabulary_)
    text_tfidf_test = TfidfTransformer(smooth_idf=False).fit(text_test)
    test_test = text_tfidf_test.transform(text_test)
    return mnb.predict_proba(test_test.toarray())


# In[105]:


prob =predictTest(sys.argv[1])


# In[113]:


best_n = np.argsort(prob.ravel())[::-1][:5]


# In[118]:


best_n


# In[128]:


listIntent=list()
for intent in best_n:
    listIntent.append({"intent":labels[intent],"probability":prob[0][intent]});


# In[126]:

print(json.dumps(listIntent))
sys.stdout.flush()