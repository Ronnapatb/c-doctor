'use strict';
const axios = require('axios');
let express = require('express'),
  bodyParser = require('body-parser'),
  app = express(),
  request = require('request'),
  config = require('config');

var contextList = [];
const waitFor = (ms) => new Promise(r => setTimeout(r, ms));
// var headache = {
//   contextInput: "",
//   contextOutput: "riskSymptoms",
//   lifespan: 2,
//   trainingMessage: "รู้สึกปวดหัวจังเลยครับ",
//   responseMessage: [{
//     type: 1,
//     text: "มีอาการอย่างใดอย่างหนึ่งในนี้หรือไม่ครับ",
//     button: [{
//         "type": "postback",
//         "title": "มีอาการปวดศรีษะครั้งแรกหลังอายุ 50 ปี",
//         "payload": "Yes"
//       },
//       {
//         "type": "postback",
//         "title": "มีอาการปวดศรีษะแบบรุนแรงเฉียบพลัน",
//         "payload": "Yes"
//       },
//       {
//         "type": "postback",
//         "title": "มีไข้และคอแข็ง",
//         "payload": "Yes"
//       },
//       {
//         "type": "postback",
//         "title": "มีอาการปวดมากขึ้นเรื่อยๆและไม่มีทีท่าจะลดลง",
//         "payload": "Yes"
//       },
//       {
//         "type": "postback",
//         "title": "ไม่มี",
//         "payload": "No"
//       },
//     ]
//   }],
// }
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.listen(8989, () => {
  console.log('Example app listening on port 8989!');
});

app.get('/', (req, res) => res.send('Hello World!'));

// Adds support for GET requests to our webhook
app.get('/webhook', (req, res) => {

  // Your verify token. Should be a random string.
  let VERIFY_TOKEN = "C_Doctor";

  // Parse the query params
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];

  // Checks if a token and mode is in the query string of the request
  if (mode && token) {

    // Checks the mode and token sent is correct
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {

      // Responds with the challenge token from the request
      console.log('WEBHOOK_VERIFIED');
      res.status(200).send(challenge);

    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
});
// Creates the endpoint for our webhook
app.post('/webhook', (req, res) => {

  let body = req.body;

  if (body.object === 'page') {

    // Iterates over each entry - there may be multiple if batched
    body.entry.forEach(function (entry) {

      // Gets the message. entry.messaging is an array, but
      // will only ever contain one message, so we get index 0

      let webhook_event = entry.messaging[0];
      console.log(webhook_event);
      console.log(entry);

      // Get the sender PSID
      let sender_psid = webhook_event.sender.id;
      console.log('Sender PSID: ' + sender_psid);
      if (contextList.length == 0) {
        contextList.push({
          senderId: sender_psid,
          context: "",
          parameter: []
        })
        console.log("complete add in if")
      }
      else {
        contextList.forEach(function (id) {
          console.log(id);
          if (sender_psid == id.senderId) {
            console.log("ID EXIST");
          }
          else {
            contextList.push({
              senderId: sender_psid,
              context: "",
              parameter: []
            })
            console.log("complete add in else")

          }
        });
      }


      // Check if the event is a message or postback and
      // pass the event to the appropriate handler function
      if (webhook_event.message) {
        console.log("message")
        handleMessage(sender_psid, webhook_event.message);
      } else if (webhook_event.postback) {
        console.log("postback")
        handlePostback(sender_psid, webhook_event.postback);
      }
    });

    // Returns a '200 OK' response to all requests
    res.status(200).send('EVENT_RECEIVED');
  } else {
    // Returns a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
  }

});
function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}
// Handles messages events
const handleMessage = (sender_psid, received_message) => {
  // let response;
  // var maximum = 0;
  // var intent;
  var modelData =""
  var finish = false;
  contextList.forEach(function (id) {
    console.log(id.context)
    if (sender_psid == id.senderId) {
      axios.post('http://localhost:7777/getIntent',{"contextInput":id.context}).then(function (response) {
        let nlpDataset = [];
        console.log(response.data)
        // nlpDataset.push(response.data);
        // nlpDataset.push(received_message.text);
        const spawn = require("child_process").spawn;
        const pythonProcess = spawn('python3', ["predictModel.py", received_message.text]);

        pythonProcess.stdout.on('data', (data) => {
          let intentList = JSON.parse(data)
          console.log(intentList)
          
          for(var j =0; j< intentList.length ; j++){
            // console.log(intentList[j])
            for(var i =0;i< response.data.length ; i++){
              if(intentList[j].probability > 0.4){
                if (intentList[j].intent == response.data[i]._id ) {
                  if(response.data[i].parameter.length>0)
                  {
                    response.data[i].parameter.forEach(parameter => {
                      if(parameter.variable!="" && parameter.value!=""){
                        id.parameter.push(parameter);
                      }
                    });
                    
                  }
                  
                  if (response.data[i].sendApi == true) {
                    console.log("send Api intent")
                    id.parameter.forEach(userParameter => {
                      modelData += userParameter.variable + "=" + userParameter.value + "&"
                    });
                    console.log(modelData)
                    axios.get('https://stark-oasis-75400.herokuapp.com/api?' + modelData).then(function (responseApi) {
                      console.log(responseApi.data)
                      var result
                      var highestProb = 0;
                      responseApi.data.forEach(diseaseList => {
                        if (diseaseList.probabilities > highestProb) {
                          result = diseaseList.disease
                          highestProb = diseaseList.probabilities
                        }
                      });
                      if (response.data[i].endConver == true) {
                        axios.get('http://localhost:7777/getResult/' + result).then(function (response) {
                          console.log(response)
                          sendResponseMessage(response.data.responseMessage,sender_psid)
                        });
                        id.parameter = [];
                      }
                      else if (response.data[i].endConver == false && highestProb > 0.7) {
                        axios.get('http://localhost:7777/getResult/' + result).then(function (response) {
                          sendResponseMessage(response.data.responseMessage,sender_psid)
                        });
                      }
                    })
                  }
                  else {
                    
                    console.log("send normal intent")
                    sendResponseMessage(response.data[i].responseMessage,sender_psid)
                    id.context = response.data[i].contextOutput
                  }
                  finish = true;
                  break;
                }
              }

              else {
                for(i<)
                console.log("fallback intent")
                axios.get('http://localhost:7777/getFallbackIntent').then(function (response) {
                  sendResponseMessage(response.data.responseMessage,sender_psid)
                });
                finish = true;
                break;
              }

            }
            if(finish == true){
              break;
            }
          }

        });




        //   axios.post('http://127.0.0.1:5000/test',nlpDataset).then(function (intent) {
        //     if(intent.data.endConver == true){
        //       axios.get('https://stark-oasis-75400.herokuapp.com/api?'+ modelData).then(function (response){
        //         id.context = ""
        //         var disease = ""
        //         modelData = {};
        //         if(response.data.migraine > response.data.tension ){
        //           var disease = "มีโอกาสเป็นอาการของ ไมเกรน - Migraine"
        //         }
        //         else{
        //           var disease = "มีโอกาสเป็นอาการของ เทนชั่น - tension"
        //         }
        //         var result = [{text:"ผลการวินิจฉัยออกแล้วนะครับ"},{text:disease},{text:response.data.cause},{text:"วิธีการดูแลรักษาเบื้องต้น"},{text:response.data.treatment},{text:"จากอาการข้างต้นผมแนะนำให้ไปพบแพทย์เพิ่มเติมนะครับผม"},
        //       {text:"หากมีอาการอย่างไรอีกบอกผมมาได้ตลอดเลยนะครับ ขอบคุณมากครับ :)"}]
        //         callSendAPI(sender_psid, result,7,0,1);
        //         //console.log(response.data)
        //       })

        //     }
        //     else{
        //       if(intent.data == "error"){
        //         callSendAPI(sender_psid, [{text: "ผมไม่เข้าใจที่คุณพูดครับ เอาใหม่ได้ไหมครับ"}],1,0,1);
        //       }
        //       else{
        //         id.context = intent.data.context.output
        //     console.log(intent.data);
        //     if(!isEmpty(intent.data.modelData)){
        //       console.log("-------------------")
        //       if(isEmpty(modelData)){
        //         modelData = intent.data.modelData.variable + "=" + intent.data.modelData.value
        //       }
        //       else{
        //         modelData += "&" + intent.data.modelData.variable + "=" + intent.data.modelData.value
        //       }

        //     }
        //     intent.data.responseMessage.forEach(function (res){
        //       if( res.type == 1){
        //         maximum += 1;
        //       }
        //     })
        //     callSendAPI(sender_psid, intent.data.responseMessage,maximum,0,1);
        //     intent.data.responseMessage.forEach(function (res){
        //         if(res.type == 2){
        //         callSendAPI(sender_psid, askTemplate(res.text, res.button),0,0,2)
        //       }
        //     })
        //       }
        //     }



        // })

      })
    }

  });


}
const sendResponseMessage = (response,sender_psid) => {
  if (response.length > 0) {
    callSendAPI(sender_psid, response, response.length - 1, 0)
  }
  else {
    console.log("no response Message")
  }
}
// 
const handlePostback = (sender_psid, received_postback) => {
  let response;
  var count;
  var maximum = 0;
  // Get the payload for the postback
  let payload = received_postback.payload;
  contextList.forEach(function (id) {
    console.log(id.context)
    if (sender_psid == id.senderId) {

      //console.log("modelData = %s",modelData);
      //https://stark-oasis-75400.herokuapp.com/api?Gender=0&Education=0&Occupation=0&PainLevel=0&PainFrequency=0&PainLocation=0
      axios.post('http://localhost:7777/getIntent', { context: id.context }).then(function (response) {
        let nlpDataset = [];
        //console.log(response.data)
        nlpDataset.push(response.data);
        nlpDataset.push(payload);
        axios.post('http://127.0.0.1:5000/test', nlpDataset).then(function (intent) {
          if (intent.data.endConver == true) {
            axios.get('https://stark-oasis-75400.herokuapp.com/api?' + modelData).then(function (response) {
              id.context = ""
              var disease = ""
              modelData = {};
              if (response.data.migraine > response.data.tension) {
                var disease = "มีโอกาสเป็นอาการของ ไมเกรน - Migraine"
              }
              else {
                var disease = "มีโอกาสเป็นอาการของ เทนชั่น - tension"
              }
              var result = [{ text: "ผลการวินิจฉัยออกแล้วนะครับ" }, { text: disease }, { text: response.data.cause }, { text: "วิธีการดูแลรักษาเบื้องต้น" }, { text: response.data.treatment }, { text: "จากอาการข้างต้นผมแนะนำให้ไปพบแพทย์เพิ่มเติมนะครับผม" },
              { text: "หากมีอาการอย่างไรอีกบอกผมมาได้ตลอดเลยนะครับ ขอบคุณมากครับ :)" }]
              callSendAPI(sender_psid, result, 7, 0, 1);
              //console.log(response.data)
            })

          }
          else {
            if (intent.data == "error") {
              callSendAPI(sender_psid, [{ text: "ผมไม่เข้าใจที่คุณพูดครับ เอาใหม่ได้ไหมครับ" }], 1, 0, 1);
            }
            else {
              id.context = intent.data.context.output
              console.log(intent.data);
              if (!isEmpty(intent.data.modelData)) {
                console.log("-------------------")
                if (isEmpty(modelData)) {
                  modelData = intent.data.modelData.variable + "=" + intent.data.modelData.value
                }
                else {
                  modelData += "&" + intent.data.modelData.variable + "=" + intent.data.modelData.value
                }

              }
              intent.data.responseMessage.forEach(function (res) {
                if (res.type == 1) {
                  maximum += 1;
                }
              })
              callSendAPI(sender_psid, intent.data.responseMessage, maximum, 0, 1);
              intent.data.responseMessage.forEach(function (res) {
                if (res.type == 2) {
                  callSendAPI(sender_psid, askTemplate(res.text, res.button), 0, 0, 2)
                }
              })
            }
          }



        })

      })
      console.log(modelData)

    }

  });

}
const askTemplate = (text, button) => {
  var value = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "button",
        "text": text,
        "buttons": []
      }
    }
  }
  var justArray = [];
  for (var i = 0; i < button.length; i++) {
    var element = {};
    element["type"] = "postback";
    element["title"] = button[i];
    element["payload"] = button[i];
    // console.log(element)
    value.attachment.payload.buttons.push(element);

  }
  return value
}

const imageTemplate = (image) => {
  var value = {
    "attachment": {
      "type": "image",
      "payload": {
        "is_reusable": true,
        "url": image
      }
    }
  }
  // var justArray = [];
  // for (var i = 0; i < button.length; i++) {
  //   var element = {};
  //   element["type"] = "postback";
  //   element["title"] = button[i];
  //   element["payload"] = button[i];
  //   // console.log(element)
  //   value.attachment.payload.buttons.push(element);

  // }
  return value
}

// Sends response messages via the Send API
const callSendAPI = (sender_psid, response, maximum, count) => {
  // Construct the message body
  let request_body = {
    "recipient": {
      "id": sender_psid
    },
  };
  var type = response[count].type

  if (type == 1) {
    request_body.message = { "text": response[count].text[Math.floor(Math.random() * response[count].text.length)] }
  }
  else if (type == 2) {
    request_body.message = imageTemplate(response[count].image);
  }
  console.log("maximum = %d count = %d", maximum, count)

  // Send the HTTP request to the Messenger Platform
  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": {
      "access_token": config.get('facebook.page.access_token')
    },
    "method": "POST",
    "json": request_body
  }, (err, res, body) => {
    if (!err) {
      if (maximum != count) {
        callSendAPI(sender_psid, response, maximum, count + 1)
      }

    } else {
      console.error("Unable to send message:" + err);
    }
  });

}