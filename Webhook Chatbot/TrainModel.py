#!/usr/bin/env python
# coding: utf-8

# In[121]:


import pymongo
import json
import numpy as np
from pymongo import MongoClient


##connect to datavase
client = MongoClient('mongodb://localhost:27017/') 
db = client.intent

collection = db.intent  ## reference collection and ensure to pluralise collection name by adding "s" at the end of collection name
arrayData = list(collection.find({ "type": "custom" }))
arrayData


# In[122]:


csvArray = []
for data in arrayData:
    for trainingData in data["trainingData"]:
        csvArray.append({"text": trainingData,"intent": str(data["_id"])})
csvArray


# In[123]:


import glob
import codecs
import numpy
import pandas as pd
from pandas import DataFrame
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import KFold
from sklearn.metrics import confusion_matrix, f1_score
from pandas import DataFrame
data = DataFrame(csvArray)
data = data.append([data]* 5,ignore_index=True)


# In[124]:


data


# In[125]:


## Tokenizer


# In[126]:


from itertools import chain
from pythainlp import word_tokenize


def tokenize_text_newmm(l):
    """Tokenize list of text"""
    return list(chain.from_iterable([word_tokenize(l, engine='newmm') ]))

tokenized_texts_train = data.text.map(tokenize_text_newmm)


# In[127]:


tokenized_texts_train


# In[128]:


from pythainlp.corpus import stopwords

# stop_word = stopwords.words('thai')
other = ['\r',' ',"ครับ","ค่ะ","คะ","ค่า","คับ","ครัช","นะ"]




# In[129]:


def clean_list(tokenized_texts):

    i = 0

    for i in range(len( tokenized_texts)):
        if (i % 10 == 0):
            print("Cleaned "+ str(i) +" lines ")
        # tokenized_texts[i] = [word for word in tokenized_texts[i] if word not in stop_word]
        tokenized_texts[i] = [word for word in tokenized_texts[i] if word not in other]


# In[130]:


clean_list(tokenized_texts_train)


# In[131]:


vocabulary_ = {v: k for k, v in enumerate(set(chain.from_iterable(tokenized_texts_train)))}


# In[132]:


vocabulary_


# In[133]:


import scipy.sparse as sp


def text_to_bow(tokenized_text, vocabulary_):
  
    n_doc = len(tokenized_text)
    values, row_indices, col_indices = [], [], []
    for r, tokens in enumerate(tokenized_text):
        feature = {}
        for token in tokens:
            word_index = vocabulary_.get(token)
            if word_index is not None:
                if word_index not in feature.keys():
                    feature[word_index] = 1
                else:
                    feature[word_index] += 1
        for c, v in feature.items():
            values.append(v)
            row_indices.append(r)
            col_indices.append(c)

    # document-term matrix in sparse CSR format
    X = sp.csr_matrix((values, (row_indices, col_indices)),
                      shape=(n_doc, len(vocabulary_)))
    return X
X = text_to_bow(tokenized_texts_train, vocabulary_)


# In[134]:


X.toarray()


# In[135]:


##TF-IDF


# In[136]:


from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.decomposition import TruncatedSVD
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.naive_bayes import GaussianNB, BernoulliNB, MultinomialNB
# transformer = TfidfTransformer(smooth_idf=False)
# svd_model = TruncatedSVD(n_components=5,
#                          algorithm='arpack', n_iter=100)
X_tfidf = TfidfTransformer(smooth_idf=False).fit(X)
messages_tfidf = X_tfidf.transform(X)
#X_svd = svd_model.fit_transform(X_tfidf)
y = data.iloc[:]['intent'].values

#X_train, X_test, y_train, y_test = train_test_split(X_svd, y)


# In[137]:


print (messages_tfidf.shape)


# In[138]:


y = pd.get_dummies(y)
labels = y.columns.values.tolist()
y = y.values


# In[139]:


mnb = MultinomialNB()
mnb.fit(messages_tfidf,y.argmax(axis=1).reshape(-1,1))


# In[140]:


def predictTest(Text):
    test_Sentence_token = word_tokenize(Text, engine='newmm') 

    text_test = text_to_bow([test_Sentence_token], vocabulary_)
    text_tfidf_test = TfidfTransformer(smooth_idf=False).fit(text_test)
    test_test = text_tfidf_test.transform(text_test)
    return mnb.predict_proba(test_test.toarray())


# In[141]:


prob = predictTest("อยากรู้เรื่องปวดหัว")


# In[142]:


prob


# In[143]:


best_n = np.argsort(prob.ravel())[::-1][:5]


# In[144]:


best_n


# In[145]:


labels[4]


# In[146]:


text ="รู้สึกปวดหัวมาก 2 เลย"
'ปวดหัวมาก 2' in text


# In[147]:


mnb.classes_


# In[155]:


from sklearn.externals import joblib 
joblib.dump(mnb, 'test.pkl')
joblib.dump(vocabulary_, 'vocab.pkl') 
joblib.dump(labels, 'labels.pkl') 


# In[149]:


# from sklearn.model_selection import train_test_split

# X_train, X_test, y_train, y_test = train_test_split(messages_tfidf, y.argmax(axis = 1).reshape(-1, 1), test_size=0.2, random_state=0, stratify=y.argmax(axis = 1).reshape(-1, 1))


# In[150]:


# mnb = MultinomialNB()
# mnb.fit(X_train,y_train)


# In[151]:


# print('Accuracy of NB classifier on test set with TF-IDF: {:.5f}'
#      .format(mnb.score(X_test, y_test)))


# In[152]:


# y_pred = mnb.predict_proba(X_test)
# y_pred


# In[153]:


# from sklearn.metrics import classification_report
# print(classification_report(y_test, y_pred.argmax(1).reshape(-1, 1)))


# In[ ]:




