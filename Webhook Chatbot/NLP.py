from pythainlp.tokenize import word_tokenize
import sys
from flask import Flask,jsonify,request
import nltk
nltk.download('omw')
from nltk.stem.lancaster import LancasterStemmer
app = Flask(__name__)
@app.route("/test", methods=["GET","POST"])
def test():
    
    class_word = {}
    high_class = None
    high_score = 0

    keepWordToken = []
    #testIntent = {
    #    'text' : ["ดี","จ้า","สวัสดี","โย่ว"]
    #}
    
    receiveData = request.json
    classes = list([a['class'] for a in receiveData[0]])
    for c in classes:
        class_word[c] = []
    print(classes)
    for data in receiveData[0]:
        for sentence in data['trainingData']:
            for word in word_tokenize(sentence):
                class_word[data['class']].append(word)
    
    print(class_word)
    for className in class_word.keys():
        # check to see if the stem of the word is in any of our classes
        print(className)
        score = 0
        for word in word_tokenize(receiveData[1]):
            print("word : %s" % word)
            if word in class_word[className]:
                # treat each word with same weight
                score += 1
                print ("   match: %s" % word)
        if score > high_score:
            high_class = className
            high_score = score
        print("score: %s" % score )
    
    for data in receiveData[0]:
        if data['class'] == high_class:
            return jsonify(data)
            
    return jsonify("error")
            
    #for intent in receiveData[0]:
    #    for trainData in intent:
    #        trainData.trainingData
    #    receiveData.append({class:})
   # print(text_from_node_server[0][0]);
    #cutText=word_tokenize(text_from_node_server,engine='deepcut')
    #for word in cutText:
    #    if word in testIntent['text']:
    #        score += 1
    #        print("   match: %s" % word )






app.run()

