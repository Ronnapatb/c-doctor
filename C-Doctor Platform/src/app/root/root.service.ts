import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RootService {

  constructor(private http: HttpClient) { }
  addNewIntent(newAddIntent){
    return this.http.post('http://localhost:7777/addNewIntent',newAddIntent);
  }
  getAllIntent(type){
    return this.http.get('http://localhost:7777/getAllIntent/'+type);
  }
  addNewFollowupIntent(intent){
    console.log(intent);
    return this.http.post('http://localhost:7777/addNewFollowupIntent',intent)
  }
  updateIntent(intent){
    return this.http.post('http://localhost:7777/updateRootFollowupIntent',intent)
  }
  deleteIntent(listId:Array<String>){
    console.log(listId)
    return this.http.post('http://localhost:7777/deleteIntent',listId)
  }
  saveIntent(intent){
    console.log(intent)
    return this.http.post('http://localhost:7777/UpdateIntent',intent)
  }

}
