export interface Intent {
    name:"newIntent",
    followUpIntent:[],
    root:true,
    contextInput:"",
    contextOutput:"",
    trainingData:[],
    parameter:[
    {
      variable:"",
      value: "",
    }
      ],
    responseMessage:[]
}