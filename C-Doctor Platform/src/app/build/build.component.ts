import { Component, OnInit,TemplateRef } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { RootService } from '../root/root.service'
import * as $ from 'jquery';
import {Intent} from './intent';
@Component({
  selector: 'app-build',
  templateUrl: './build.component.html',
  styleUrls: ['./build.component.scss']
})
export class BuildComponent implements OnInit {
  closeResult: string;
  public name:String ;
  fallbackIntent: any;
  test = true;
  intentForm = {
    name:"newIntent",
    followUpIntent:[],
    type: null,
    diseaseResult: null,
    root:true,
    contextInput:"",
    contextOutput:"",
    trainingData:[],
    parameter:[
    {
      variable:"",
      value: "",
    }
      ],
    responseMessage:[],
    endConver: false,
    sendApi:false
  }
  intent:any = null;
  public data=[];
  public selectedIntent:String;
  //       {
  //         type:1,
  //         text:"this is a text of button template",
  //         variable:"",
  //         button:[
  //           {
  //             //"type": "postback",
  //             "title": "Button1",
  //             "payload": "",
  //             "value":"",
  //           },
  //           {
  //             //"type": "postback",
  //             "title": "Button2",
  //             "payload": "",
  //             "value":"",
  //           },
            
  //         ]
  //       }
  public cloneModalData = null;
  // public currentIndex:number;
  // public currentIndexButton:number;
  // public currentButton:object = null;
  trainingData:Array<String> = [];
  responseMessage:Array<any> = [];
  listIntent:any;
  url = '';
  isOver=false;
  constructor(private modalService: NgbModal,private rootService : RootService,) {

   }

  newTrainingData(event) {

    this.intent.trainingData.unshift(event.target.value);
    event.target.value = null;

  }
  ngOnInit() {
    
    this.rootService.getAllIntent({}).subscribe((response:Array<any>)=>{
      console.log('response from post data is ', response);
      this.data = [...response]
      this.showBlockList();
    },(error)=>{
      console.log('error during post is ', error)
    })
    this.rootService.getAllIntent("fallback").subscribe((response:Array<any>)=>{
      console.log('response from post data is ', response);
      this.fallbackIntent = response
    },(error)=>{
      console.log('error during post is ', error)
    })
    
    this.name = "Chatbot Conversation" 
    console.log(this.trainingData[0]);
  }
  showBlockList(){
    var cloneData;
    var mockData= {
      _id: 1234,
      name:"main",
      type: "custom",
      diseaseResult: null,
      root:false,
      contextInput:"",
      contextOutput:"",
      trainingData:[],
      parameter:[
      {
        variable:"",
        value: ""
      }
      ],
      responseMessage:[{
        type:1,
        text:["ดีจ้าาาาา"],
        image:""
      }],
      followUpIntent:[],
      endConver: false,
      sendApi:false
    }
    cloneData = [...this.data];
    cloneData.forEach(function (mock){
      if(mock.root== true)
      {
        mockData.followUpIntent.push(mock._id)
      }
    })
    cloneData.unshift(mockData);
    console.log(cloneData)
    //console.log(this.data)
    var data = [...cloneData],
    tree = function (array) {
        var nodes = Object.create(null),
            r = {};
            data.forEach(function (a) {
            if (!nodes[a._id]) {
                nodes[a._id] = { 
                  _id: a._id,
                  name:a.name, 
                  followUpIntent:[],
                  type: a.type,
                  diseaseResult: a.diseaseResult,
                  root:a.root,
                  contextInput: a.contextInput,
                  contextOutput: a.contextOutput,
                  trainingData:[],
                  parameter: a.parameter,
                  responseMessage:a.responseMessage,
                  endConver: a.endConver,
                  sendApi:a.sendApi
                };
                r = nodes[a._id];
            }
            a.followUpIntent.forEach(function (b) {
              for(var i=0; i<data.length;i++){
                if(b==data[i]._id){
                  nodes[b] = { 
                    _id: data[i]._id, 
                    name: data[i].name,
                    followUpIntent:[],
                    type: data[i].type,
                    diseaseResult: data[i].diseaseResult,
                    root:data[i].root,
                    contextInput: data[i].contextInput,
                    contextOutput: data[i].contextOutput,
                    trainingData: data[i].trainingData,
                    parameter: data[i].parameter,
                    responseMessage:data[i].responseMessage,
                    endConver: data[i].endConver,
                    sendApi:data[i].sendApi
                  };
                  //console.log(nodes[b])
                  nodes[a._id].followUpIntent.push(nodes[b]);
                  break;
                }
              }
                
            });
        });
        return r;
    }(data);
    console.log(tree);
    this.listIntent=  JSON.parse(JSON.stringify(tree));
    console.log(this.listIntent);
  }
  register(form) {
    console.log(form.value);
    console.log(this.intent)
    this.intent.name = form.value.name;
    this.intent.contextInput = form.value.contextInput;
    this.intent.contextOutput = form.value.contextOutput;
    for(var i=0;i<this.data.length;i++){
      if(this.data[i]._id == this.intent._id){
        this.data.splice(i,1,this.intent)
        break;
      }
    }
    this.rootService.saveIntent(this.intent).subscribe(()=>{
    })
    this.showBlockList();
    
  }
  addResponseMessage(type){
    if(type==1)
    {
      this.intent.responseMessage.push({
        type:1,
        text:[""],
        image:""
      })
    }
    else if(type==2)
    {
      this.intent.responseMessage.push({
        type:2,
        text:[],
        image:""
      })
    }
  }
  addText(index,event){
    this.intent.responseMessage[index].text = event.target.value
    console.log(this.intent)
  }
  deleteResponseMessage(index){
    this.intent.responseMessage.splice(index,1);
  }
  deleteTrainingData(index){
    this.intent.trainingData.splice(index,1);
  }

  setMyStyles(index){
    let styles = {
      'border-bottom': '0px',
    };
    if(index === 0){
      return styles
    }
  }
  result(value){
    this.intent = null;
    console.log(value);
    for(var i=0; i<this.data.length;i++){
      if(this.data[i]._id == value._id){
        this.intent = JSON.parse(JSON.stringify(this.data[i]))
        this.selectedIntent = this.intent._id
        
        break;
      }
    }
    console.log(this.intent);
  }
  addFollowUpIntent(item){
    console.log(item)
    var currentItem
    for(var i =0;i<this.data.length;i++){
      if(this.data[i]._id== item){
        currentItem = JSON.parse(JSON.stringify(this.data[i]));
        break;
      }
    }
    this.rootService.addNewFollowupIntent(currentItem).subscribe((data)=>{
      this.data[i].followUpIntent.push(data["_id"]);
      this.data[i].contextOutput = data["contextInput"];
      // this.intent.followUpIntent.push(data["_id"])
      // this.intent.contextOutput = data["contextInput"];
      this.intent = JSON.parse(JSON.stringify(this.data[i]));
      this.data.push(data);
      this.rootService.updateIntent(this.data[i]).subscribe(()=>{
      })
      this.showBlockList();
    })
    
    console.log(this.data)
  }
  idForDelete:Array<String> = []
  async deleteIntent(item){
    console.log(item)
    var idChild:Array<number>= new Array;
    this.idForDelete.splice(0,this.idForDelete.length)
        for(var i=0;i<this.data.length;i++){
          for(var j=0;j<this.data[i].followUpIntent.length;j++)
          {
            if(this.data[i]._id == this.intent._id){
              this.intent =null;
            }
            if(this.data[i].followUpIntent[j] == item._id)
            {
              this.data[i].followUpIntent.splice(j,1)
              this.rootService.updateIntent(this.data[i]).subscribe(()=>{

              })
              break;
            }
          }
          
        }
        this.idForDelete.push(item._id)
        item.followUpIntent.forEach(element => {
          idChild.push(element._id)
        });
        console.log(idChild)
        await this.deleteRecursion(idChild)
        console.log(this.idForDelete)
        this.rootService.deleteIntent(this.idForDelete).subscribe(()=>{
        })
        for(var counter=this.data.length - 1; counter >= 0;counter--){
            for(var i=0;i<=this.idForDelete.length;i++){
              if(this.data[counter]._id == this.idForDelete[i]){
                this.data.splice(counter,1);
                break;
              }
            }
          }
          console.log(this.data)

          this.showBlockList();
  }
  deleteRecursion(child){
    var dataClone = JSON.parse(JSON.stringify(this.data));
    console.log(dataClone)
    console.log(child)
    for(var i=0;i<dataClone.length;i++){
      for(var j=0;j<child.length;j++){
        if(child[j] == dataClone[i]._id){
          this.idForDelete.push(child[j]);
          if(dataClone[i].followUpIntent[0] != undefined){
            this.deleteRecursion(dataClone[i].followUpIntent);
          }
        }
      }
    }
    console.log(this.idForDelete)
  }
  addNewIntent(type:number){
    var newAddIntent = JSON.parse(JSON.stringify(this.intentForm));
    if(type ==1){
      newAddIntent.type = "custom"
    }
    else if(type ==2){
      newAddIntent.name = "Tension Result"
      newAddIntent.type = "result"
      newAddIntent.diseaseResult = "tension"
    }
    else if(type ==3){
      newAddIntent.name = "Migraine Result"
      newAddIntent.type = "result"
      newAddIntent.diseaseResult =  "migraine"
    }
    this.rootService.addNewIntent(newAddIntent).subscribe((response)=>{
      console.log('response from post data is ', response);
      this.data.push(response);
      this.showBlockList();
    },(error)=>{
      console.log('error during post is ', error)
    })

  }
onURLinserted(index,event) {
    this.intent.responseMessage[index].image =  event.target.value;
}
editTrainingData(i,event){
console.log(event.target.value)
this.intent.trainingData[i]=event.target.value;
}
trackByFn(index, item) {
  return index;  }
editVariable(index,event){
  this.intent.parameter[index].variable = event.target.value;
}
editValue(index,event){
  this.intent.parameter[index].value = event.target.value;
}
autoGrowTextZone(e) {
  e.target.style.height = "0px";
  e.target.style.height = (e.target.scrollHeight + 2)+"px";
}
editTextResponse(iMain,i,event){
this.intent.responseMessage[iMain].text[i] = event.target.value
console.log(this.intent.responseMessage[iMain])
}
createNewRow(iMain,i){
  this.intent.responseMessage[iMain].text.splice(i,0,"");
}
addTextResponse(iMain){
  this.intent.responseMessage[iMain].text.push("");
}
deleteTextMessage(iMain,textIndex){
  this.intent.responseMessage[iMain].text.splice(textIndex,1);
}
addNewParameter(){
  var parameter={
      variable:"",
      value: ""
  }
  this.intent.parameter.push(parameter);
}
doalert(checkboxElem,index) {
  console.log(checkboxElem.target.checked)
  console.log(this.intent)
  // if (checkboxElem.target.checked) {
  //   this.intent.parameter[index].require = true;
  // } else {
  //   this.intent.parameter[index].require = false;
  // }
}
open(content) {
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return  `with: ${reason}`;
  }
}
getFallbackIntent(){
  this.intent = null;
  this.rootService.getAllIntent("fallback").subscribe((response:Object)=>{
    console.log('response from post data is ', response);
    this.intent = JSON.parse(JSON.stringify(response[0]));
    this.selectedIntent = this.intent._id
  },(error)=>{
    console.log('error during post is ', error)
  })
}
getWelcomeIntent(){
  this.intent = null;
  this.rootService.getAllIntent("welcome").subscribe((response:Object)=>{
    console.log('response from post data is ', response);
    this.intent = JSON.parse(JSON.stringify(response[0]));
    this.selectedIntent = this.intent._id
  },(error)=>{
    console.log('error during post is ', error)
  })
}

}
