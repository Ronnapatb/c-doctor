import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Node } from '../build/node.model';
@Component({
  selector: 'app-recursion',
  templateUrl: './recursion.component.html',
  styleUrls: ['./recursion.component.scss']
})
export class RecursionComponent implements OnInit {
  @Input() data:any;
  @Input() isHideSingle:boolean;
  @Input() isHide:Array<boolean>=[true,true,true,true,true,true,true];
  @Input() selectedIntent:String;
  @Output() clickEvent = new EventEmitter();
  @Output() addEvent = new EventEmitter();
  @Output() deleteEvent = new EventEmitter();
  @Output() reSelectedIntent = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }
  hideIntent(index){
    this.isHide[index] = !this.isHide[index]
    console.log(this.isHide)
  }
  onClick(item) {
    console.log("clicked", item);
    this.clickEvent.emit(item);
    //this.selectedIntent = item._id;
  }
  onClickChild(item) {
    this.clickEvent.emit(item);
    //this.selectedIntent = item._id;
    console.log(item)
  }
  addFollowUpIntent(item){
    console.log(item)
    this.addEvent.emit(item._id);
  }
  addFollowUpIntentChild(item){
    this.addEvent.emit(item);
  }
  deleteIntent(item){
    this.deleteEvent.emit(item);
  }
  deleteIntentChild(item){
    this.deleteEvent.emit(item);
  }
  onSelect(id:String): void {
    //this.selectedIntent = id;
    //console.log(this.selectedIntent === id);
  }
  // private isLeaf(): boolean {
  //   return this.node.nodes.length === 0;
  // }
}
